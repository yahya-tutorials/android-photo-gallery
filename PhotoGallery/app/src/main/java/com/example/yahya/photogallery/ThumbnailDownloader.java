package com.example.yahya.photogallery;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by Yahya on 11/10/2017.
 */

public class ThumbnailDownloader<T> extends HandlerThread
{
    public static final String TAG = "ThumbnailDownloader";
    public static final int MESSAGE_DOWNLOAD = 0; //message's what
    private Handler mRequestHandler;
    private ConcurrentMap<T, String> mRequestMap = new ConcurrentHashMap<>();
    private Handler mResponseHandler; //Pointer to main threads handler
    private ThumbnailDownloaderListener<T> mThumbnailDownloaderListener;

    public void setThumbnailDownloaderListener(ThumbnailDownloaderListener<T> ThumbnailDownloaderListener)
    {
        this.mThumbnailDownloaderListener = ThumbnailDownloaderListener;
    }

    public interface ThumbnailDownloaderListener<T>
    {
        void onThumbnailDownloaded(T target, Bitmap thumbnail);
    }

    public ThumbnailDownloader(Handler responseHandler)
    {
        super(TAG);
        mResponseHandler = responseHandler;
    }

    @Override
    protected void onLooperPrepared()
    {
        mRequestHandler = new Handler()
        {
            @Override
            public void handleMessage(Message msg)
            {
                if (msg.what == MESSAGE_DOWNLOAD)
                {
                    T target = (T) msg.obj;
                    Log.i(TAG, "Got a request for URL :" + mRequestMap.get(target));
                    handleRequest(target);
                }
            }
        };


    }

    private void handleRequest(final T target)
    {
        final String url = mRequestMap.get(target);
        if (url == null)
            return;
        try
        {
            byte[] bitmapBytes = new FlickerFetchr().getUrlBytes(url);
            final Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
            Log.i(TAG, "Bitmap created");

            mResponseHandler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    if(mRequestMap.get(target) != url)
                    {
                        return;
                    }

                    mRequestMap.remove(target);
                    mThumbnailDownloaderListener.onThumbnailDownloaded(target, bitmap);

                }
            });
        }
        catch (IOException ioe)
        {
            Log.e(TAG, "Error downloading image", ioe);
        }

    }

    public void queueThubnail(T target, String url)
    {
        Log.i(TAG, "Got a URL" + url);
        if (url == null)
        {
            mRequestMap.remove(target);
        }
        else
        {
            mRequestMap.put(target, url);
            mRequestHandler.obtainMessage(MESSAGE_DOWNLOAD, target).sendToTarget();
        }
    }

    public void clearQueue()
    {
        mResponseHandler.removeMessages(MESSAGE_DOWNLOAD);
    }
}
