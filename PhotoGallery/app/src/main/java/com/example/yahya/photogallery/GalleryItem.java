package com.example.yahya.photogallery;

import android.net.Uri;

/**
 * Created by Yahya on 11/1/2017.
 */

public class GalleryItem
{
    private String mCaption;
    private String mId;
    private String mUrl;
    private String mDataOwner;

    public String getDataOwner()
    {
        return mDataOwner;
    }

    public void setDataOwner(String dataOwner)
    {
        mDataOwner = dataOwner;
    }

    public void setmCaption(String mCaption)
    {
        this.mCaption = mCaption;
    }

    public Uri getPhotoPageUri()
    {
        return Uri.parse("http://www.flicker.com/photos")
                .buildUpon()
                .appendPath(mDataOwner)
                .appendPath(mId)
                .build();
    }

    public void setmId(String mId)
    {
        this.mId = mId;
    }

    public void setmUrl(String mUrl)
    {
        this.mUrl = mUrl;
    }

    public String getmCaption()
    {

        return mCaption;
    }

    public String getmId()
    {
        return mId;
    }

    public String getmUrl()
    {
        return mUrl;
    }

    @Override
    public String toString()
    {
        return mCaption;
    }
}
