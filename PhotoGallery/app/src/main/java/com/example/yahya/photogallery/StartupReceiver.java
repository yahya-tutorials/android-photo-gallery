package com.example.yahya.photogallery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

/**
 * Created by Yahya on 2/4/2018.
 */

public class StartupReceiver extends BroadcastReceiver
{
    public static final String TAG = "StartupReceiver";
    @Override
    public void onReceive(Context context, Intent intent)
    {
        boolean isOn = QueryPreference.isAlarmOn(context);
        /*
        Commented out for the sake of fast debugging
         */
        //if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        //{
            PollIntentService.setServiceAlarm(context, isOn);
        //}
        //else
        //{
        //    if(isOn)
        //    {
        //        PollJobService.start(context);
        //    }
        //}
        Log.i(TAG, "Received broadcast intent " + intent.getAction());
    }
}
