package com.example.yahya.photogallery;

import android.app.Activity;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

/**
 * Created by Yahya on 2/8/2018.
 */

public class NotificationReceiver extends BroadcastReceiver
{
    public static final String TAG = "NotificationReceiver";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.i(TAG, "received result : " + getResultCode());
        if(getResultCode() != Activity.RESULT_OK)
        {
            //a foreground activity cancelled the broadcast
            return;
        }

        int requestCode = intent.getIntExtra(PollIntentService.REQUEST_CODE, 0);
        Notification notification =(Notification) intent.getParcelableExtra(PollIntentService.NOTIFICATION);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(requestCode, notification);
    }
}
