package com.example.yahya.photogallery;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Yahya on 2/6/2018.
 */

public abstract class VisibleFragment extends Fragment
{
    private static final String TAG = "VisibleFragment";

    @Override
    public void onStart()
    {
        super.onStart();
        IntentFilter filter = new IntentFilter(PollIntentService.ACTION_SHOW_NOTIFICATION);
        filter.addAction(PollIntentService.ACTION_SHOW_NOTIFICATION);
        getActivity().registerReceiver(mOnShowNotification, filter);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        getActivity().unregisterReceiver(mOnShowNotification);
    }

    private BroadcastReceiver mOnShowNotification = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            //Toast.makeText(getActivity(), "Got a broadcast: " + intent.getAction(), Toast.LENGTH_LONG).show();
            /*
            If we receive this, we're visible, so cancel the notification
             */
            Log.i(TAG, "cancelling notification");
            setResultCode(Activity.RESULT_CANCELED);
        }
    };
}
