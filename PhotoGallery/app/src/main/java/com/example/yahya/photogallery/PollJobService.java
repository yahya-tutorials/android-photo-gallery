package com.example.yahya.photogallery;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.List;

/**
 * Created by Yahya on 1/24/2018.
 */

public class PollJobService extends JobService
{
    private static final String TAG = "PollJobService";
    private static final String ACTION_SHOW_NOTIFICATION = "com.example.yahya.photogallery.SHOW_NOTIFICATION";
    private PollTask mCurrentTask;

    public static void start(Context context)
    {
        final int JOB_ID = 1;
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

        /*
        Check whether the job has already been scheduled
         */
        boolean hasBeenScheduled = false;
        for (JobInfo jobInfo : scheduler.getAllPendingJobs())
        {
            if (jobInfo.getId() == JOB_ID)
            {
                hasBeenScheduled = true;
            }
        }

        if (!hasBeenScheduled)
        {
            JobInfo jobInfo;
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//            {
//                jobInfo = new JobInfo.Builder(JOB_ID, new ComponentName(context, PollJobService.class))
//                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
//                        .setMinimumLatency(1000)
//                        //.setPeriodic(2000)
//                        .setPersisted(true)
//                        .build();
//            }
//            else
//            {
            jobInfo = new JobInfo.Builder(JOB_ID, new ComponentName(context, PollJobService.class))
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPeriodic(1000 * 60 * 15)
                    .setPersisted(true)
                    .build();
//            }
            scheduler.schedule(jobInfo);
        }

    }

    @Override
    public boolean onStartJob(JobParameters params)
    {
        mCurrentTask = new PollTask(this);
        mCurrentTask.execute(params);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params)
    {
        return false;
    }

    /**
     * @return true if there exists a network permission with connected network available, false otherwise
     */
    private boolean isNetworkAvailableAndConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        boolean isNetworkAvailable = cm.getActiveNetwork() != null;
        boolean isNetworkConnected = isNetworkAvailable && cm.getActiveNetworkInfo().isConnected();
        return isNetworkConnected;
    }

    private class PollTask extends AsyncTask<JobParameters, Void, Void>
    {
        private Context context;

        public PollTask(Context context)
        {
            this.context = context;
        }

        @Override
        protected Void doInBackground(JobParameters... jobParameters)
        {
            JobParameters jobParam = jobParameters[0];
            if (isNetworkAvailableAndConnected())
            {
                //Log.i(TAG, "Received an intent: " + intent);
                String query = QueryPreference.getStoredQuery(context);
                String lastResultId = QueryPreference.getLastResultId(context);

                List<GalleryItem> items;

                if (query == null) // No query entered, show the recently updated photos
                {
                    items = new FlickerFetchr().fetchRecentPhotos();
                }
                else // There exists a query, search for it
                {
                    items = new FlickerFetchr().searchPhotos(query);
                }

                if (items.size() > 0)
                {


                    String resultId = items.get(0).getmId();
                    if (resultId.equals(lastResultId)) //If the result is the same as the previous fetch
                    {
                        Log.i(TAG, "Got an old result " + resultId);
                    }
                    else //Result Id is not equal to the previous fetch and hence new results emerged
                    {
                        Log.i(TAG, "Got a new result: " + resultId);
                        Resources resources = getResources();
                        Intent i = PhotoGalleryActivity.newIntent(context);
                        PendingIntent pi = PendingIntent.getService(context, 0, i, 0);

                        /*
                        Creating notification manager based on the version of the android
                         */
                        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        NotificationCompat.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            /*
                            Creating notification channel for >= Android O  versions
                             */
                            NotificationChannel notificationChannel = new NotificationChannel("ID", "My Notifications", NotificationManager.IMPORTANCE_DEFAULT);

                            // Configure the notification channel.
                            notificationChannel.setDescription("Channel description");
                            notificationChannel.enableLights(true);
                            notificationChannel.setLightColor(Color.RED);
                            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                            notificationChannel.enableVibration(true);
                            notificationManager.createNotificationChannel(notificationChannel);
                            builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
                        }
                        else
                        {
                            /*
                            Deprecated notificaiton builder for < Android O
                             */
                            builder = new NotificationCompat.Builder(getApplicationContext());
                        }

                        /*
                        Creating the notification itself
                         */
                        builder = builder
                                .setTicker(resources.getString(R.string.new_pictures_title))
                                .setSmallIcon(android.R.drawable.ic_menu_report_image)
                                .setContentTitle(resources.getString(R.string.new_pictures_title))
                                .setContentText(resources.getString(R.string.new_pictures_text))
                                .setContentIntent(pi)
                                .setAutoCancel(true)
                                .setDefaults(Notification.DEFAULT_ALL);


                         /*
                         Showing the notification
                          */
                        notificationManager.notify(0, builder.build());

                        /*
                        Sending notification broadcast
                         */
                        sendBroadcast(new Intent(ACTION_SHOW_NOTIFICATION));
                    }
                    QueryPreference.setLastResultId(context, resultId);
                }
            }
            jobFinished(jobParam, false);
            return null;
        }
    }
}
