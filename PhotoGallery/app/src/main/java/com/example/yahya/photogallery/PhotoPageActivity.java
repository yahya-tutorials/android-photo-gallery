package com.example.yahya.photogallery;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by Yahya on 2/13/2018.
 */

public class PhotoPageActivity extends SingleFragmentActivity
{
    private static final String TAG = "PhotoPageActivity";
    private PhotoPageFragment mPhotoPageFragment;
    public static Intent newInent(Context context, Uri pageUri)
    {
        Intent i = new Intent(context, PhotoPageActivity.class);
        i.setData(pageUri);
        return i;
    }

    @Override
    protected Fragment createFragment()
    {
        mPhotoPageFragment = PhotoPageFragment.newInstance(getIntent().getData());
        return mPhotoPageFragment;
    }

    @Override
    public void onBackPressed()
    {
        Log.i(TAG, "onBackPressed");
        if (mPhotoPageFragment.backButtonPressed() < 0)
        {
            super.onBackPressed();
        }
    }
}
