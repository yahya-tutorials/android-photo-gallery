package com.example.yahya.photogallery;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Yahya on 10/30/2017.
 */

public class PhotoGalleryFragment extends VisibleFragment
{
    private static final String TAG = "PhotoGalleryFragment";
    private RecyclerView mPhotoRecyclerView;
    private List<GalleryItem> mItems = new ArrayList<>();
    private ThumbnailDownloader<PhotoHolder> mThumbnailDownloader;
    private View mProgressView;

    public static PhotoGalleryFragment newInstance()
    {
        return new PhotoGalleryFragment();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mThumbnailDownloader.quit();
        Log.i(TAG, "Background thread downloader");
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        mThumbnailDownloader.clearQueue();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        //updateItems();

        /*
        Running the PollIntentService
         */
        //Intent i = PollIntentService.newIntent(getActivity());
        //getActivity().startService(i);

        /*
        Commented out for the sake of fast debugging
         */
        //if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        //{
            PollIntentService.setServiceAlarm(getActivity(), true);
        //}
        //else
        //{
        //    PollJobService.start(this.getActivity());
        //}
        Handler responseHandler = new Handler();
        mThumbnailDownloader = new ThumbnailDownloader<>(responseHandler);
        mThumbnailDownloader.setThumbnailDownloaderListener(
                new ThumbnailDownloader.ThumbnailDownloaderListener<PhotoHolder>()
                {
                    @Override
                    public void onThumbnailDownloaded(PhotoHolder target, Bitmap thumbnail)
                    {
                        Drawable drawable = new BitmapDrawable(getResources(), thumbnail);
                        target.bingGalleryItem(drawable);
                    }
                }
        );
        mThumbnailDownloader.start();
        mThumbnailDownloader.getLooper();
        Log.i(TAG, "Background thread started");

        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_photo_gallery, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            /**
             *
             * @param query submitted query
             * @return true if it handled the submitted query, false otherwise
             */
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                Log.d(TAG, "Query text submit: " + query);
                QueryPreference.setStoredQeury(getActivity(), query);
                updateItems();
                /*
                Collapse the search view after a search was done
                 */
                searchView.setIconified(true);
                return true;
            }

            /**
             *
             * @param newText new text entered into the search view
             * @return true if it handled the submitted query, false otherwise
             */
            @Override
            public boolean onQueryTextChange(String newText)
            {
                Log.d(TAG, "Query text changed: " + newText);
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String query = QueryPreference.getStoredQuery(getActivity());
                searchView.setQuery(query, false);
            }
        });

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            MenuItem toggleItem = menu.findItem(R.id.menu_item_toggle_polling);
            if (PollIntentService.isServiceAlarmOn(getActivity()))
            {
                toggleItem.setTitle(R.string.stop_polling);
            }
            else
            {
                toggleItem.setTitle(R.string.start_polling);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_item_clear:
                QueryPreference.setStoredQeury(getActivity(), null);
                updateItems();
                return true;
            case R.id.menu_item_toggle_polling:
                boolean shouldStartAlarm;
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                {
                    shouldStartAlarm = !PollIntentService.isServiceAlarmOn(getActivity());
                    PollIntentService.setServiceAlarm(getActivity(), shouldStartAlarm);
                }
                else
                {
                    shouldStartAlarm = true;
                }

                getActivity().invalidateOptionsMenu();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateItems()
    {
        String query = QueryPreference.getStoredQuery(getActivity());
        new FetchItemTask(query).execute();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_photo_gallery, container, false);
        mPhotoRecyclerView = v.findViewById(R.id.fragment_photo_gallery_recycler_view);
        mProgressView = v.findViewById(R.id.fragment_photo_gallery_progress_view);
        mPhotoRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        setAdapter();

        updateItems();
        return v;
    }

    private void setAdapter()
    {
        if (isAdded())
        {
            mPhotoRecyclerView.setAdapter(new PhotoAdapter(mItems));
        }
    }

    private void showProgressBar(boolean show)
    {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private class FetchItemTask extends AsyncTask<Void, Void, List<GalleryItem>>
    {
        private String mQuery;

        public FetchItemTask(String mQuery)
        {
            this.mQuery = mQuery;
        }

        @Override
        protected List<GalleryItem> doInBackground(Void... params)
        {


            if (mQuery == null)
            {
                return new FlickerFetchr().fetchRecentPhotos();

            }
            else
                return new FlickerFetchr().searchPhotos(mQuery);
        }

        @Override
        protected void onPostExecute(List<GalleryItem> items)
        {
            showProgressBar(false);
            mItems = items;
            setAdapter();
        }

        @Override
        protected void onPreExecute()
        {
            showProgressBar(true);
        }
    }

    private class PhotoHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private ImageView mImageView;
        private GalleryItem mGalleryItem;

        @Override
        public void onClick(View v)
        {
            //Intent i = new Intent(Intent.ACTION_VIEW, mGalleryItem.getPhotoPageUri());
            Intent i = PhotoPageActivity.newInent(getActivity(), mGalleryItem.getPhotoPageUri());
            startActivity(i);
        }

        /*
                Connects to the layout
                 */
        public PhotoHolder(View itemView)
        {
            super(itemView);
            mImageView = itemView.findViewById(R.id.fragment_photo_gallery_image_view);
            itemView.setOnClickListener(this);
        }

        public void bindGalleryItem(GalleryItem galleryItem)
        {
            mGalleryItem = galleryItem;
        }

        /*
        Sets the attributes of the layout
         */
        public void bingGalleryItem(Drawable drawable)
        {
            mImageView.setImageDrawable(drawable);
        }
    }

    private class PhotoAdapter extends RecyclerView.Adapter<PhotoHolder>
    {
        private List<GalleryItem> mGalleryItems;

        public PhotoAdapter(List<GalleryItem> mGalleryItems)
        {
            this.mGalleryItems = mGalleryItems;
        }

        @Override
        public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(R.layout.gallery_item, parent, false);
            return new PhotoHolder(view);
        }

        @Override
        public void onBindViewHolder(PhotoHolder holder, int position)
        {
            GalleryItem galleryItem = mGalleryItems.get(position);
            Drawable placeholder = getResources().getDrawable(R.drawable.ku);
            holder.bingGalleryItem(placeholder);
            holder.bindGalleryItem(galleryItem);
            mThumbnailDownloader.queueThubnail(holder, galleryItem.getmUrl());
        }

        @Override
        public int getItemCount()
        {
            return mGalleryItems.size();
        }

    }


}
