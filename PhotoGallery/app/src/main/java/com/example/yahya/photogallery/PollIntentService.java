package com.example.yahya.photogallery;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;

import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.Nullable;


import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.List;

/**
 * Created by Yahya on 1/1/2018.
 */

public class PollIntentService extends IntentService
{
    private static final String TAG = "PollIntentService";
    public static final String REQUEST_CODE = "REQUEST_CODE";
    public static final String NOTIFICATION = "NOTIFICATION";
    public static final String PERM_PRIVATE = "com.example.yahya.photogallery.PRIVATE";
    private static final int POLL_INTERVAL = 1000 * 60; // 60 seconds
    public static final String ACTION_SHOW_NOTIFICATION = "com.example.yahya.photogallery.SHOW_NOTIFICATION";
    public static Intent newIntent(Context context)
    {
        return new Intent(context, PollIntentService.class);
    }

    public PollIntentService()
    {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent)
    {
        if(!isNetworkAvailableAndConnected())
        {
            return;
        }
        //Log.i(TAG, "Received an intent: " + intent);
        String query = QueryPreference.getStoredQuery(this);
        String lastResultId = QueryPreference.getLastResultId(this);

        List<GalleryItem> items;

        if(query == null) // No query entered, show the recently updated photos
        {
            items = new FlickerFetchr().fetchRecentPhotos();
        }
        else // There exists a query, search for it
        {
            items = new FlickerFetchr().searchPhotos(query);
        }

        if(items.size() == 0)
        {
            return;
        }

        String resultId = items.get(0).getmId();
        if(resultId.equals(lastResultId)) //If the result is the same as the previous fetch
        {
            Log.i(TAG, "Got an old result " + resultId);
        }
        else //Result Id is not equal to the previous fetch and hence new results emerged
        {
            Log.i(TAG, "Got a new result: " + resultId);
            Resources resources = getResources();
            Intent i = PhotoGalleryActivity.newIntent(this);
            PendingIntent pi = PendingIntent.getService(this, 0, i, 0);

            /*
            Creating notification manager based on the version of the android
             */
            NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                /*
                Creating notification channel for >= Android O  versions
                 */
                NotificationChannel notificationChannel = new NotificationChannel("ID", "My Notifications",  NotificationManager.IMPORTANCE_DEFAULT);

                // Configure the notification channel.
                notificationChannel.setDescription("Channel description");
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                notificationChannel.enableVibration(true);
                notificationManager.createNotificationChannel(notificationChannel);
                builder = new NotificationCompat.Builder(getApplicationContext(), notificationChannel.getId());
            }
            else
            {
                /*
                Deprecated notificaiton builder for < Android O
                 */
                builder = new NotificationCompat.Builder(getApplicationContext());
            }

            /*
            Creating the notification itself
             */
             builder = builder
                    .setTicker(resources.getString(R.string.new_pictures_title))
                    .setSmallIcon(android.R.drawable.ic_menu_report_image)
                    .setContentTitle(resources.getString(R.string.new_pictures_title))
                    .setContentText(resources.getString(R.string.new_pictures_text))
                    .setContentIntent(pi)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL);


             /*
             Showing the notification
              */
            //notificationManager.notify(0, builder.build());
            Notification notification = builder.build();
            /*
            Sending notification broadcast
             */
            //sendBroadcast(new Intent(ACTION_SHOW_NOTIFICATION), PERM_PRIVATE);
            showBackgroundNotification(0, notification);
        }
        QueryPreference.setLastResultId(this, resultId);
    }

    private void showBackgroundNotification(int requestCode, Notification notification)
    {
        Intent i = new Intent(ACTION_SHOW_NOTIFICATION);
        i.putExtra(REQUEST_CODE, requestCode);
        i.putExtra(NOTIFICATION, notification);
        sendOrderedBroadcast(i, PERM_PRIVATE, null, null, Activity.RESULT_OK, null, null);
    }

    /**
     *
     * @return true if there exists a network permission with connected network available, false otherwise
     */
    private boolean isNetworkAvailableAndConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        boolean isNetworkAvailable = cm.getActiveNetwork() != null;
        boolean isNetworkConnected = isNetworkAvailable && cm.getActiveNetworkInfo().isConnected();
        return isNetworkConnected;
    }

    public static void setServiceAlarm(Context context, boolean isOn)
    {
        Intent i = PollIntentService.newIntent(context);
        PendingIntent pi = PendingIntent.getService(context, 0, i, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if(isOn)
        {
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), POLL_INTERVAL, pi);
        }
        else
        {
            alarmManager.cancel(pi);
            pi.cancel();
        }
        QueryPreference.setAlarmOn(context, isOn);
    }

    /**
     *
     * @param context the caller class
     * @return true if there exists any pending intent on the unique pair of (context, 0, i), false otherwise where i is the intent of
     * PollIntentService.java
     */
    public static boolean isServiceAlarmOn(Context context)
    {
        Intent i = PollIntentService.newIntent(context);
        PendingIntent pi = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_NO_CREATE);
        return pi != null;
    }
}
