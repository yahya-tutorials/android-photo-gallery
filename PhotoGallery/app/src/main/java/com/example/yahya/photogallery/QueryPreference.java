package com.example.yahya.photogallery;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by Yahya on 12/27/2017.
 */

public class QueryPreference
{
    private static final String PREF_SEARCH = "searchQuery";
    private static final String PREF_LAST_RESULT_ID = "lastResultId";
    private static final String PREF_IS_ALARM_ON = "isAlarmOn";

    public static String getStoredQuery(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(PREF_SEARCH, null);
    }

    public static void setStoredQeury(Context context, String query)
    {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(PREF_SEARCH, query)
                .apply();
    }

    /**
     *
     * @param context caller activity (this, or getActivity for activity and fragments, respectively)
     * @return the value of the shared last result ID preference, or null if there wasn't such yet
     */
    public static String getLastResultId(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(PREF_LAST_RESULT_ID, null);
    }

    /**
     *
     * @param context caller activity (this, or getActivity for activity and fragments, respectively)
     * @param query the query string that is going to be put in the last result ID preference
     */
    public static void setLastResultId(Context context, String query)
    {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(PREF_LAST_RESULT_ID, query)
                .apply();
    }

    /**
     *
     * @param context caller activity (this, or getActivity for activity and fragments, respectively)
     * @return the status of alarm that is either on or off
     */
    public static boolean isAlarmOn(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(PREF_IS_ALARM_ON, false);
    }

    /**
     *
     * @param context context caller activity (this, or getActivity for activity and fragments, respectively)
     * @param isOn the status of alarm that is going to be set, either on or off
     */
    public static void setAlarmOn(Context context, boolean isOn)
    {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(PREF_IS_ALARM_ON, isOn)
                .apply();
    }
}
